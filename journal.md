2017-10-27
Added react-script to allow auto-loading of changes in browser (it's awesome)
Added some of the designed objects into village.BUILDINGS. Prototyped the building stock updates and display by using document.write. Will convert this to React later.
Should add a building type so the BUILDINGS object can be filtered depending on which building element is being populated (production, storage, survival)

2017-10-28
* Today, going to convert the document.write to react elements
* To start, will just use one react element called Building; it will take a standard building object. Loop through the buildings and react.render each one.
* ES6 timer example: https://codepen.io/mirkof/pen/RRVamN

update:
* React components are in. Added a timer. Started work on resource flow.
* Have the consume materials/produce goods logic working, but now need to work on how to get items out of produced goods into the warehouse. I thought I could use the same logic and assign a villager to the storage building, where after progress reaches 100%, an item is moved from wherever it is produced (iron/smithy, wood/forester, leather/(hunter or pasture)) to the storage unit, but I think the storage unit has to know about these sources, and the names of the dicts are slightly different (adding to materials instead of producedGoods, or taking from producedGoods instead of materials to move from storage unit to target building.) Might need completely separate logic for storage units.

2017-10-29
* Reorg of structure; moved materials and producedGoods counters into a state variable rather than having the individual buildings keep track. This may get reverted later to implement the restocking requirement, but for now, there is a general stock pool that all produced goods go into and all materials are consumed from.

2017-10-30
* Did a code review, deleted some dead code
* Fixed a bug where if first materials needed to produce is missing, but second is available, material is still produced
* Added villager list generation
* Next step: villager assignment handling

2017-10-31
* Implemented villager object assignment to buildings; used filter and find operations on the Villager array to handle determining how many unassigned, and first unassigned
* Next: have tool affect production rate

2017-11-01
* Added more knobsAndLevers values
* Tools now affect production rate
* Tools now degrade and break
* Added pretty colors to indicate production status
* Added sorting and priority adjustment; it's somewhat fragile: if there is no building with a priority 1 higher, the building will not move; this means if building a has priority 5 and building b has priority 10, nothing will happen when the up/down buttons are pushed. If there is any break in the sequence of priorities in the buildings array, reassignment of priority will not work. A better approach would be to find the next highest value.
  * Fixed this by using filter and reduce

2017-11-03
* Visual changes
  * Updated the css formatting
  * Added villager components
  * Used gradients to show durability of tools and progress of production

2017-11-05
* Implemented warmth mechanic
  * Villager consumes firewood to increase warmth stat
* Implemented energy mechanic
  * Villager consumes food to increase energy stat

2017-11-06
* added messaging
* slimmed up villager and building elements

2017-11-07
* more style redesigns to take better advantage of real estate. Can now see all pertinent information simultaneously on mobile screen. It spreads out a little too much in the browser, but that's OK. Really hoping I won't have to have different elements for desktop browser to make it responsive.

2017-11-08
TODO
* Add varying production amounts, rather than just having each building produce 1 of each of its goods each round. This will make it so the gatherer's hut isn't useless by allowing it to produce 2 food. (this could be accomplished by just adding food to the gatherer's hut twice. This would have to be accounted for in the displaying of the goods in the gatherer's hut building element.