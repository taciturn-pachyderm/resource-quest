# An automated resource/village manager

## Running the game

```npm install```

```npm start```

This should load a browser window and navigate to http://localhost:3000 automatically.

## Core game mechanic(s)

Keep the villagers alive. If all villagers die the game is over.

Each 'building' produces a widget. Each widget has a perk for being available and/or a penalty for not being available.

See https://gitlab.com/shoelaces-and-sadness/game-designs/tree/master/resource-quest for more design details.

## Publishing the game

Run `scripts/publish.sh`

This script will execute `npm run build`, push into the `dist` dir, then perform a git push to the publish repo.
