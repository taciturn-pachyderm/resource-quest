#!/usr/bin/env bash

mkdir dist

pushd dist

git init
git remote add origin git@gitlab.com:modle13/resource-quest-publish.git
git pull origin master

popd


npm run build

cp -r build/* dist


pushd dist

git add .
git commit -m 'update build'
git push --set-upstream origin master

popd
